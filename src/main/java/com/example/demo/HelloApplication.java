package com.example.demo;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.IOException;

/*
* JavaFX project structure is like this in one stage we can have many scenes, like broswer has tabs
* Inside the scenes we can have many layouts and inside the layouts we have buttons
* With buttons we can label it and while users are making some actions with the buttons like, hoober or click
* then an event is occured and we have to handle the event in the handle method by overriding it from the
* Eventhandler interface. other than that we are using the main method for launching the event and extending the
* start method from the Application class
* */



public class HelloApplication extends Application implements EventHandler<ActionEvent>{
    Button button;
    Label selectionMsg = new Label("Your selection: None");
    public static void main(String[] args) {
        launch();
    }


//    public void start(Stage stage) throws IOException {
//        // Stage which is given as a parameter
//        stage.setTitle("Hello!!!!");
//        //Creating a button && setting the button text
//        button = new Button();
//        button.setText("Click me!");
//        Label buttonLabel = new Label("Label for Button", button);
//        button.setOnAction(this);
//        TextField textField = new TextField("Text Field");
//        Label textFieldlabel = new Label("textFieldlabel", textField);
//        TextField textField1 = new TextField("Text Field1");
//        Label textFieldlabel1 = new Label("textFieldlabel1", textField1);
//
//        //Adding a line
//        Line line = new Line();
//        line.setStartX(-100);
//        line.setStartY(-100);
//        line.setEndX(0);
//        line.setEndY(0);
//        Line line1 = new Line(10,50,150,50);
//        Line line2 = new Line(10,100,150,100);
//        Line line3 = new Line(10,50,10,100);
//        Line line4 = new Line(150,50,150,100);
//        Rectangle rectangle = new Rectangle(-100, -100, 0, 0);
//        rectangle.setArcHeight(35);
//        rectangle.setArcWidth(35);
//        rectangle.setFill(Color.RED);
//        Label myLabel = new Label("This is a rectangle", rectangle);
//
//        //Creating a layout and adding the elements like button, line etc. to the layout
//        StackPane layout = new StackPane();
//        textFieldlabel.setMnemonicParsing(true);
//        textFieldlabel1.setMnemonicParsing(true);
//        layout.getChildren().add(button);
////        layout.getChildren().add(buttonLabel);
//        layout.getChildren().add(textField);
////        layout.getChildren().add(textField1);
//        layout.getChildren().add(textFieldlabel);
////        layout.getChildren().add(textFieldlabel1);
////        layout.getChildren().add(line);
////        layout.getChildren().addAll(line1, line2,line3, line4);
////        layout.getChildren().addAll(rectangle);
////        layout.getChildren().add(myLabel);
////        layout.setStyle("-fx-background-color: pink");
//
//
//        //Adding the layout to the scene
//        Scene scene = new Scene(layout, 320, 240, Color.GRAY);
//        //Adding the scene to the stage
//        stage.setScene(scene);
//        //Finally showing the stage
//        stage.show();
//    }
    public void start(Stage stage) {
        ToggleButton csButton = new ToggleButton("Computer Science");
        ToggleButton pButton = new ToggleButton("Physics");
        ToggleButton chemButton = new ToggleButton("Chemistry");
        ToggleButton mButton = new ToggleButton("maths");

        final ToggleGroup group = new ToggleGroup();

        group.getToggles().addAll(csButton, pButton, chemButton, mButton);

        group.selectedToggleProperty().addListener(
                new ChangeListener<Toggle>() {
                    @Override
                    public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle new_toggle) {
                        String toggleBtn = ((ToggleButton)new_toggle).getText();
                        selectionMsg.setText("Yourr Selection: " + toggleBtn);
                    }
                }
        );
        Label selectLbl = new Label("Select the subject: ");
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(csButton, pButton, chemButton, mButton);
        buttonBox.setSpacing(10);
        VBox root = new VBox();
        root.getChildren().addAll(selectionMsg, selectLbl, buttonBox);
        root.setSpacing(10);
        root.setMinSize(350, 200);
        root.setStyle("-fx-padding: 10;"
                + "-fx-border-style: solid inside;"
                + "-fx-border-width: 2;"
                + "-fx-border-insets: 5;"
                + "-fx-border-radius: 5;"
                + "-fx-border-color: blue;");
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("A ToggleButton Example");
        stage.show();
    }
    public void handle(ActionEvent event) {
        if(event.getSource() == button) {
            System.out.println("Hey!!!");
        }
    }

}